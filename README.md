# Cryptography

|          |Available|Last update|
|----------|:-------:|:---------:|
|Exams     |yes      |2020       |
|Notes     |yes      |2019       |
|References|no       |           |

## Notes

|Author  |Last update|Format   |Reference|
|--------|:---------:|:-------:|:-------:|
|Andrea  |2019       |TeX / PDF|[Source code](https://github.com/Project2100/Cryptography-2018_19)|
|Luis    |2019       |TeX / PDF|[Source code](https://github.com/Gold3nboy/cryptography_1819)|
|Michele |2017       |TeX / PDF|n/a|
